wget -O install_salt.sh https://bootstrap.saltstack.com
sudo sh install_salt.sh
sudo sed -i -e 's/#file_client: remote/file_client: local/g' /etc/salt/minion
sudo service salt-minion restart

sudo mkdir -p /srv/salt/
sudo touch /srv/salt/docker.sls
sudo tee -a /srv/salt/docker.sls <<EOF
import-docker-key:
  cmd.run:
    - name: apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    - creates: /etc/apt/sources.list.d/docker.list
/etc/apt/sources.list.d/docker.list:
  file.managed:
    - source: salt://docker.list

docker:
  pkg.installed:
    - name: docker-engine
  service.running:
    - name: docker
    - require:
      - pkg: docker-engine
EOF

sudo touch /srv/salt/docker.list
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /srv/salt/docker.list

sudo touch /srv/salt/top.sls
sudo tee -a /srv/salt/top.sls <<EOF
base:
  '*':
    - docker
EOF

sudo salt-call state.highstate
sudo docker run hello-world
docker -v
