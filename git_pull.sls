/srv/salt:
  file.directory:
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

git_pull:
  git.latest:
    - name: https://larsappel@bitbucket.org/larsappel/masterless-salt.git
    - target: /srv/salt
    - force_clone: True
