docker:
  pkgrepo:
    - managed
    - humanname: Docker Repo
    - name: deb https://apt.dockerproject.org/repo ubuntu-xenial main
    - keyserver: hkp://p80.pool.sks-keyservers.net:80
    - keyid: 58118E89F3A912897C070ADBF76221572C52609D
  pkg.installed:
    - name: docker-engine
  service.running:
    - name: docker
    - enable: True
    - require:
      - pkg: docker-engine
